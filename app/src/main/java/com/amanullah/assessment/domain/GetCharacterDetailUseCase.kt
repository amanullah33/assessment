package com.amanullah.assessment.domain

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.base.interactor.UseCase
import com.amanullah.assessment.data.remote.models.SingleCharacterAPIRequest
import com.amanullah.assessment.data.remote.models.SingleCharacterDetailsAPIResponseModel
import com.amanullah.assessment.data.remote.repository.Repository
import javax.inject.Inject

class GetCharacterDetailUseCase @Inject constructor(private val repository: Repository) :
    UseCase<SingleCharacterDetailsAPIResponseModel, SingleCharacterAPIRequest>() {
    override suspend fun run(params: SingleCharacterAPIRequest): AppResult<SingleCharacterDetailsAPIResponseModel> =
        repository.getSingleCharacterDetails(params)
}