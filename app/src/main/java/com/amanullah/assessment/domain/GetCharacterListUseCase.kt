package com.amanullah.assessment.domain

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.base.interactor.UseCase
import com.amanullah.assessment.data.remote.models.CharactersListAPIRequest
import com.amanullah.assessment.data.remote.models.CharactersListAPIResponse
import com.amanullah.assessment.data.remote.repository.Repository
import javax.inject.Inject

class GetCharacterListUseCase @Inject constructor(private val repository: Repository) :
    UseCase<CharactersListAPIResponse, CharactersListAPIRequest>() {
    override suspend fun run(params: CharactersListAPIRequest): AppResult<CharactersListAPIResponse> =
        repository.getAllCharacters(params)
}