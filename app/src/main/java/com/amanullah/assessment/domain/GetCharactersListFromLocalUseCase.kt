package com.amanullah.assessment.domain

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.base.interactor.UseCase
import com.amanullah.assessment.data.local.repository.LocalCache
import com.amanullah.assessment.data.remote.models.CharactersListAPIResponse
import javax.inject.Inject

class GetCharactersListFromLocalUseCase @Inject constructor(private val localCache: LocalCache) :
    UseCase<CharactersListAPIResponse, UseCase.None>() {
    override suspend fun run(params: None): AppResult<CharactersListAPIResponse> =
        localCache.getCharactersListFromLocal()
}