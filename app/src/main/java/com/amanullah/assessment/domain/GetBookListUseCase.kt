package com.amanullah.assessment.domain

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.base.interactor.UseCase
import com.amanullah.assessment.data.remote.models.BookListAPIRequest
import com.amanullah.assessment.data.remote.models.BookListAPIResponse
import com.amanullah.assessment.data.remote.repository.Repository
import javax.inject.Inject

class GetBookListUseCase @Inject constructor(private val repository: Repository) :
    UseCase<BookListAPIResponse, BookListAPIRequest>() {
    override suspend fun run(params: BookListAPIRequest): AppResult<BookListAPIResponse> =
        repository.getBookList(params)
}