package com.amanullah.assessment.domain

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.base.interactor.UseCase
import com.amanullah.assessment.data.local.models.CacheCharactersListRequestModel
import com.amanullah.assessment.data.local.repository.LocalCache
import javax.inject.Inject

class CacheCharactersListToLocalUseCase @Inject constructor(private val localCache: LocalCache) :
    UseCase<Boolean, CacheCharactersListRequestModel>() {
    override suspend fun run(params: CacheCharactersListRequestModel): AppResult<Boolean> =
        localCache.cacheCharactersListLocally(params)
}