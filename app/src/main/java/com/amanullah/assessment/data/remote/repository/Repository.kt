package com.amanullah.assessment.data.remote.repository

import com.amanullah.assessment.base.appresult.AppResult
import com.amanullah.assessment.data.remote.models.BookListAPIRequest
import com.amanullah.assessment.data.remote.models.BookListAPIResponse
import com.amanullah.assessment.data.remote.models.CharactersListAPIRequest
import com.amanullah.assessment.data.remote.models.CharactersListAPIResponse
import com.amanullah.assessment.data.remote.models.SingleCharacterAPIRequest
import com.amanullah.assessment.data.remote.models.SingleCharacterDetailsAPIResponseModel

/**
 * Repository that is responsible to provide data for this demo application
 * from either remote or local.
 */
interface Repository {
    /**
     * Get all Rick and Morty Characters (Paginated).
     *
     * @return [AppResult]<[CharactersListAPIResponse]>
     */
    fun getAllCharacters(request: CharactersListAPIRequest): AppResult<CharactersListAPIResponse>

    /**
     * Get single character details.
     *
     * @return [AppResult]<[SingleCharacterDetailsAPIResponseModel]>
     */
    fun getSingleCharacterDetails(request: SingleCharacterAPIRequest): AppResult<SingleCharacterDetailsAPIResponseModel>

    fun getBookList(request: BookListAPIRequest): AppResult<BookListAPIResponse>
}