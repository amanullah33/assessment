package com.amanullah.assessment.data.remote.models

import androidx.annotation.Keep
import com.amanullah.assessment.extensions.writeString
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Keep
class BookListAPIRequest(val id: Int)