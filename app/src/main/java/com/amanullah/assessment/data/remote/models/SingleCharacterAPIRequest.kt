package com.amanullah.assessment.data.remote.models

import androidx.annotation.Keep

@Keep
data class SingleCharacterAPIRequest(val id: Int)