package com.amanullah.assessment.feature.charactersview

import androidx.annotation.Keep
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.amanullah.assessment.base.uistate.BaseComposeUIState
import com.amanullah.assessment.data.remote.models.BookListAPIResponse
import com.amanullah.assessment.data.remote.models.CharactersListAPIResponse


@Keep
class CharactersViewUIState : BaseComposeUIState() {
    var dataList: MutableList<BookListAPIResponse.Companion.Book> =
        mutableStateListOf()
}