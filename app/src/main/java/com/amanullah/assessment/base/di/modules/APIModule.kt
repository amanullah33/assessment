package com.amanullah.assessment.base.di.modules

import com.amanullah.assessment.data.remote.api.APIService
import com.amanullah.assessment.data.remote.repository.Repository
import com.amanullah.assessment.data.remote.repository.RepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
object APIModule {
    @Provides
    fun provideRickMortyRepository(apiService: APIService): Repository =
        RepositoryImpl(apiService)
}