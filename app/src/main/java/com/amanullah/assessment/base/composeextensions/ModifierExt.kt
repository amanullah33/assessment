package com.amanullah.assessment.base.composeextensions

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun FillMaxWidth() = Modifier.fillMaxWidth()